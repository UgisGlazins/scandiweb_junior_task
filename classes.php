<?php
abstract class Product{
    protected $sku;
    protected $name;
    protected $price;
    protected $id;

    public function getsku(){
        return $this->sku;
    }

    public function setsku($sku){
        $this->sku = htmlspecialchars($sku);
    }

    public function getname(){
        return $this->name;
    }

    public function setname($name){
        $this->name = htmlspecialchars($name);
    }

    public function getprice(){
        return $this->price;
    }

    public function setprice($price){
        $this->price = htmlspecialchars($price);
    }
    public function getid(){
        return $this->id;
    }

    function __construct(array $arg){
        $this->id = $arg['id'] ?? 0;
        $this->sku = $arg['sku'];
        $this->name = $arg['name'];
        $this->price = $arg['price'];
    }
    function formatedOutput(){

        return $this->price . "$";
    }
    public function sqlGenerate(){
        $sql= "( ' ". $this->sku. " ', ' ". $this->name. " '," . $this->price . ", " ;
        return $sql;
    }
}



class DVD extends Product{
    private $size;
    
    public function getsize(){
        return $this->size;
    }

    public function setsize($size){
        $this->size = htmlspecialchars($size);
    }

    function formatedOutput(){
        return "Size: " . $this->size . " MB";
    }

    function __construct(array $arg){
        parent::__construct($arg);
        $this->size=$arg['size'];
    }

    public function sqlGenerate(){
        $sql="INSERT INTO products(sku, name, price, size, class) VALUES";
        $sql.= parent::sqlGenerate();
        $sql.=  $this->size .  ", 'DVD' );" ;
        return $sql;
    }
}


class Furniture extends Product{
    private $height;
    private $width;
    private $length;

    public function getheight(){
        return $this->height;
    }

    public function setheight($height){
        $this->height = htmlspecialchars($height);
    }

    public function getwidth(){
        return $this->width;
    }

    public function setwidth($width){
        $this->width = htmlspecialchars($width);
    }

    public function getlength(){
        return $this->length;
    }

    public function setlength($length){
        $this->length = htmlspecialchars($length);
    }

    function __construct(array $arg){
        parent::__construct($arg);
        $this->height=$arg['height'];
        $this->width=$arg['width'];
        $this->length=$arg['length'];
    }
    function formatedOutput(){
        return "Dimensions: " . $this->height . " x " . $this->width . " x " .$this->length;

    }

    public function sqlGenerate(){
        $sql="INSERT INTO products(sku, name, price, height, width, length, class) VALUES";
        $sql.= parent::sqlGenerate();
        $sql.= $this->height . "," . $this->width . ", " . $this->length . ", 'Furniture' );"  ;
        return $sql;
    }
}

class Book extends Product{
    private $weight;
    
    public function getweight(){
        return $this->weight;
    }

    public function setweight($weight){
        $this->weight = htmlspecialchars($weight);
    }

    function __construct(array $arg){
        parent::__construct($arg);
        $this->weight=$arg['weight'];
    }
    function formatedOutput(){
        return "Weight: " . $this->weight . " KG";
    }

    public function sqlGenerate(){
        $sql="INSERT INTO products(sku, name, price, weight, class) VALUES";
        $sql.= parent::sqlGenerate();
        $sql.=  $this->weight .  ", 'Book' );" ;
        return $sql;
    }
}
