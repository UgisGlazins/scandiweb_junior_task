<?php
include_once 'dbh.php';
include_once 'classes.php';

if (isset($_POST['remove']))
{
    $selected = $_POST['checkbox'];
    $db= new database();
    $db->massDelete($selected);
    $db->closeConnection();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="page.css">
    <title>Product List </title>
</head>
<body>
    <div class="page-container">
    <div class="header">
        <p class="header-name">Product List</p>
        <div class="header-btns">
        <button name="ADD" id="add-product-btn" type="submit"   onclick="window.location='productAdd.php'"  value="Register">
ADD</button>
<button name="remove" id="delete-product-btn" type="submit" onclick="index.php" form="remove">
MASS DELETE</button>
</div>
</div>
<div id="content-body">
<form name="remove" id="remove" method="post" action="index.php" >

<?php

$getArray = new database();
$array = $getArray->getProductArray();

foreach ($array as $object)
{

    echo '<div class="products-box">';
    echo '<input class="delete-checkbox" id="checkbox[] "type="checkbox" value="' . $object->getid() . '" name="checkbox[]" />';
    echo '<p class = "product-info" >' . $object->getsku() . '</p>';
    echo '<p class = "product-info" >' . $object->getname() . '</p>';
    echo '<p class = "product-info" >' . $object->getprice() . '</p>';
    echo '<p class = "product-info" >' . $object->formatedOutput() . '</p>';
    echo '</div>';
}

?>

</form>
</div>

<div class="footer">
   <p id="footer-text"> Scandiweb Test Assignment</p>

</div>
</div> 
</body>
</html>
