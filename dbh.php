<?php
class database
{

    private $conn;

    function __construct($host = "localhost", $username = "root", $password = "", $dbname = "product_database")
    {
        $this->conn = mysqli_connect($host, $username, $password, $dbname);
        if (!$this->conn)
        {
            echo 'Connection error!' . $this->conn;
        }
    }

    function closeConnection()
    {
        $this->conn->close();
    }

    public function getProductArray()
    {

        $array = array();
        $sql = 'SELECT*FROM products';
        $query = mysqli_query($this->conn, $sql);
        $result = mysqli_fetch_all($query, MYSQLI_ASSOC);

        foreach ($result as $object)
        {
            $product = new $object["class"]($object);
            array_push($array, $product);
        }
        return $array;
    }

    public function massDelete($array){
        {
            foreach ($array as $object)
            {
                $sql = "DELETE FROM products WHERE id=" . $object . ";";
                $this->conn->query($sql); 
            }
        }

    }

    public function insertProduct($product)
    {
        $sql = $product->sqlGenerate();
        $this->conn->query($sql);
    }

}



