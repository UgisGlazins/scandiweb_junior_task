<?php
include_once 'classes.php';
include_once 'dbh.php';
$errors = [];
if (isset($_POST['save']))
{

    $sku = htmlspecialchars($_POST['sku']);
    $name = htmlspecialchars($_POST['name']);
    $price = $_POST['price'];

    if (strlen($sku) < 5 || strlen($sku) > 10)
    {
        array_push($errors, "Please make sure that the input value is bigger than 5 and smaller than 10");
    }

    if (empty($sku) || empty($name) || empty($price))
    {
        array_push($errors, "Please fill out all of the given fields");
    }

    if (empty($errors))
    {
        $product_type = $_POST["productSelect"];
        $product = new $product_type($_POST);
        $db = new database();
        $db->insertProduct($product);
        $db->closeConnection();
        header('Location: indexo.php');
    }

}
?>


<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" type="text/css" href="page.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
      <title>Document</title>
   </head>
   <body>
      <div class="header">
         <p class="header-name">Product Add</p>
         <div class="header-btns">
            <button  class="product-add-buttons" form ="product_form" type="submit" name ="save" value="save"  >
            Save</button>
            <button name="Cancel" class="product-add-buttons" type="submit" value="cancel"  onclick="window.location='indexo.php'" >Cancel</button>
         </div>
      </div>
      <div class="form-container">
      <form id="product_form" action="productAdd.php" method="post">
         <div class="product-info-input">
            <span class="label-holder"><label for="sku" class="label">SKU</label></span>
            <span class="input-holder"><input type="text" id="sku" name="sku" ><br></span>
         </div>

         <div class="product-info-input">
         <span class="label-holder"><label for="name" class="label">Name</label></span>
          <span class="input-holder">  <input type="text" id="name" name="name"><br></span>
         </div>
         <div class="product-info-input">
         <span class="label-holder"><label for="price" class="label">Price</span></label>
         <span class="input-holder"><input type="number" id="price" name="price" min=0 value=0><br> </span>
         </div>
      
         <span class="label-holder"><label for="productSelect" class="productSelect">Type Switcher</label></span>
         <span class="input-holder">
        <select name="productSelect" id="productSelect" >
         <option value="DVD" >DVD</option>
         <option value="Furniture" >Furniture</option>
         <option value="Book"  >Book</option>
        </select>
        <br>
      </span>
        <script>
              var selector = document.getElementById("productSelect");  //Function for hiding unneccessary forms

selector.onchange =function(){                            
var options = document.querySelectorAll("div.info");      

  options.forEach(function(option){                       
    option.style.display = "none";                        

    if (option.id == selector.value) {                    
      option.style.display = "block";                    
    }

    var inputs = option.querySelectorAll("input");        
    inputs.forEach(function(input){                       
      input.value = 0;                                   
    });
  });
};
            </script>

        <div class="info"  id="DVD">
        <span class="label-holder"><label for="size" class="label">Size (MB)</label></span>
        <span class="input-holder"><input type="number" id="size" name="size" value="size" class="clear-form" min=0><br></span>
         <p id="error-output"></p>
</div>

<div class="info" style="display:none" id="Book">
<span class="label-holder"><label for="weight" class="label">Weight (KG)</label></span>
<span class="input-holder"> <input type="number" id="weight" name="weight" class="clear-form" min=0><br></span>
</div>
<div class="info" style="display:none" id="Furniture">
<span class="label-holder"><label for="height" class="label" >Height (CM)</label></span>
<span class="input-holder"> <input type="number" id="height" name="height" class="clear-form" min=0 ><br></span>
         <span class="label-holder"><label for="width" class="label">Width (CM )</label></span> 
         <span class="input-holder"><input type="number" id="width" name="width" class="clear-form" min=0><br></span>
         <span class="label-holder"><label for="length" class="label">Length (CM)</label></span>
         <span class="input-holder"> <input type="number" id="length" name="length" class="clear-form"min=0><br></span>
</div>
<p><?php
foreach ($errors as $error)
{
    echo '<br>';
    echo $error;
} ?> </p>
            </form>

      <div class="footer">
         <p id="footer-text"> Scandiweb Test Assignment</p>
      </div>
   </body>
</html>
